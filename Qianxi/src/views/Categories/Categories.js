import React, { Component } from 'react';
import { Card, Col, Row } from 'reactstrap';
import { inject, observer } from 'mobx-react';
import { Grid } from '@devexpress/dx-react-grid-material-ui';
import LoadingCircular from '../../components/LoadingCircular';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import MainTable from '../../components/TableMainComponent';
import ClearGreenButton from "../../components/GreenButton";
import { toJS } from 'mobx'
class Categories extends Component {

    componentDidMount = async () => {
        const { CategoriesStore } = this.props;
        const { getCategories } = CategoriesStore;
        await getCategories();
    }

    goToAddCategory = (history) => {
        history.push("/create/category");
    }

    renderCategoryList(history) {
        const { CategoriesStore } = this.props;
        const { categoryArray, sorting, getLoading, disabledColumn } = CategoriesStore;
        const columns = [
            { name: 'id', title: 'ID' },
            { name: 'name', title: 'Name' },
        ];
        const rows = [
            { id: "nel", name: "ho" }
        ]

        if (getLoading) {
            return (
                <Grid rows={[]} columns={columns}>
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                        <LoadingCircular />
                    </div>
                </Grid>
            );
        }

        return (
            <div>
                <BlockUi
                    blocking={getLoading}
                    loader={
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <LoadingCircular />
                        </div>
                    }
                >
                    <Card className="card-shadow" style={{ marginBottom: "10px", padding: "20px", position: "relative" }}>
                        <Row>
                            <Col style={{ width: "100%", display: "flex", justifyContent: "space-between" }}>
                                <span style={{ fontFamily: "Quicksand", fontWeight: "bold", fontSize: "30px" }}>Category List</span>
                                <ClearGreenButton
                                    buttonText={"Create Category"}
                                    buttonWidth="100px"
                                    buttonHeight="40px"
                                    onClick={() => this.goToAddCategory(history)}
                                />
                            </Col>
                        </Row>
                    </Card>
                    <Card className="card-shadow">
                        <Row>
                            <Col>
                                <Row>
                                    <Col>
                                        <MainTable
                                            tableArray={toJS(categoryArray) || []}
                                            cols={columns}
                                            sortingState={[]}
                                            pageSize={1000}
                                            dataCount={1000}
                                            loadingBool={getLoading}
                                            canClickRow={true}
                                            history={history}
                                            pageName={'categories'}
                                        />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Card>
                </BlockUi>
            </div>
        );
    }

    render() {
        const { CategoriesStore, history } = this.props;
        const { userArray } = CategoriesStore;
        return (
            <div>
                <p style={{ fontFamily: "Quicksand", fontSize: "20px", fontWeight: "bold" }}>Hello, Admin</p>
                {this.renderCategoryList(history)}
            </div>
        )
    }
}

export default inject('CategoriesStore')(observer(Categories));
