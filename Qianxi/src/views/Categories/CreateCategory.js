import React, { Component } from "react"
import { inject, observer } from 'mobx-react';
import { toJS } from 'mobx'
import { Card, Col, Row } from 'reactstrap';
import TxtField from "../../components/TxtField"
import RedButton from "../../components/RedButton"
import GreenButton from "../../components/GreenButton";
import LoadingCircular from '../../components/LoadingCircular';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';


class CategoryDetail extends Component {

    componentDidMount = async () => {
        const { CategoriesStore } = this.props;
        const { clearData } = CategoriesStore;
        await clearData();
    }

    componentWillUnmount = async () => {
        const { CategoriesStore } = this.props;
        const { clearData } = CategoriesStore;
        await clearData();
    }

    handleChange = event => {
        const { CategoriesStore } = this.props;
        const { setEditedData } = CategoriesStore;
        let value = event.target.value;
        let { name } = event.target;

        setEditedData({
            [name]: value
        })
    }

    createCategory = async () => {
        const { CategoriesStore, history } = this.props;
        const { createCategory } = CategoriesStore;
        await createCategory({ history })
    }

    render() {
        const { CategoriesStore } = this.props;
        const { editedData, getLoading } = CategoriesStore;
        return (
            <React.Fragment>
                <BlockUi
                    blocking={getLoading}
                    loader={
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <LoadingCircular />
                        </div>
                    }
                >
                    <Card className="card-shadow" style={{ padding: "5px", marginBottom: "15px" }}>
                        <Row>
                            <Col>
                                <h2 className="default-font">Create Category</h2>
                            </Col>
                        </Row>
                    </Card>
                    <Card className="card-shadow">
                        <Row  >
                            <Col sm="12" md="8" lg="8" style={{ position: "relative" }}>
                                <TxtField
                                    txtName={"name"}
                                    txtPlaceholder={"Name"}
                                    txtLabel={"Name"}
                                    txtValue={editedData.name}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}

                                />
                            </Col>
                        </Row>
                        <Row className="flex-row">
                            <Col style={{ margin: "15px" }}>
                                <GreenButton
                                    buttonText={"Save"}
                                    onClick={() => this.createCategory()}
                                    isDisabled={false}
                                    buttonWidth="100px"
                                    buttonHeight="40px"
                                />
                            </Col>
                        </Row>
                    </Card>
                </BlockUi>
            </React.Fragment>
        )
    }
}

export default inject('CategoriesStore')(observer(CategoryDetail)); 