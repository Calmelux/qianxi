import React, { Component } from "react"
import { inject, observer } from 'mobx-react';
import { toJS } from 'mobx'
import { Card, Col, Row } from 'reactstrap';
import TxtField from "../../components/TxtField"
import RedButton from "../../components/RedButton"
import GreenButton from "../../components/GreenButton";
import LoadingCircular from '../../components/LoadingCircular';
import BlockUi from 'react-block-ui';
import DropzoneImage from '../../components/Dropzone'
import 'react-block-ui/style.css';

class OrderDetail extends Component {

    componentDidMount = async () => {
        const { OrderStore } = this.props;
        const { clearData } = OrderStore;
        await clearData();
    }

    componentWillUnmount = async () => {
        const { OrderStore } = this.props;
        const { clearData } = OrderStore;
        clearData();
    }


    handleChange = event => {
        const { OrderStore } = this.props;
        const { setEditedData } = OrderStore;
        let value = event.target.value;
        let { name } = event.target;

        setEditedData({
            [name]: value
        })
    }

    createOrder = async () => {
        const { OrderStore, history } = this.props;
        const { createOrder } = OrderStore;
        await createOrder({ history });
    }

    render() {
        const { OrderStore } = this.props;
        const { editedData, getLoading } = OrderStore;
        console.log("editedData--", toJS(editedData))
        return (
            <React.Fragment>
                <BlockUi
                    blocking={getLoading}
                    loader={
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <LoadingCircular />
                        </div>
                    }
                >
                    <Card className="card-shadow" style={{ padding: "5px", marginBottom: "15px" }}>
                        <Row>
                            <Col>
                                <h2 className="default-font">Create Order</h2>
                            </Col>
                        </Row>
                    </Card>
                    <Card className="card-shadow">
                        <Row  >
                            <Col sm="12" md="8" lg="8" style={{ position: "relative" }}>
                                <TxtField
                                    txtName={"totalAmount"}
                                    txtPlaceholder={"Total Amount"}
                                    txtLabel={"Total Amount"}
                                    txtValue={editedData.totalAmount}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                    txtType={"number"}
                                />
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%", display: "flex", flexDirection: "column" }}>
                                <TxtField
                                    txtName={"productListID"}
                                    txtPlaceholder={"Product List ID"}
                                    txtLabel={"Product List ID"}
                                    txtValue={editedData.productListID + ""}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                    disableTxtField={false}
                                />
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%" }}>
                                <TxtField
                                    txtName={"quantity"}
                                    txtPlaceholder={"Quantity"}
                                    txtLabel={"Quantity"}
                                    txtValue={editedData.quantity + ""}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                />
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%" }}>
                                <TxtField
                                    txtName={"orderDate"}
                                    txtPlaceholder={"Order Date"}
                                    txtLabel={"Order Date"}
                                    txtValue={editedData.orderDate + ""}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                />
                            </Col>
                        </Row>
                        <Row className="flex-row">
                            <Col style={{ margin: "15px" }}>
                                <GreenButton
                                    buttonText={"Save"}
                                    onClick={() => this.createOrder()}
                                    isDisabled={false}
                                    buttonWidth="100px"
                                    buttonHeight="40px"
                                />
                            </Col>
                            {/* <Col style={{ margin: "15px" }}>
                                <RedButton
                                    buttonText={"Remove Product"}
                                    onClick={() => console.log("Ban!")}
                                    isDisabled={false}
                                    buttonWidth="100px"
                                    buttonHeight="40px"
                                />
                            </Col> */}
                        </Row>
                    </Card>
                </BlockUi>
            </React.Fragment>
        )
    }
}
export default inject('OrderStore')(
    observer(OrderDetail),
);