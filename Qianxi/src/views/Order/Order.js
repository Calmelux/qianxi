import React, { Component } from "react"
import { Grid, TableHeaderRow, Table } from '@devexpress/dx-react-grid-material-ui';
import { SortingState, IntegratedSorting, IntegratedFiltering } from '@devexpress/dx-react-grid';
import { withStyles } from '@material-ui/core/styles';
import LoadingCircular from '../../components/LoadingCircular';
import BlockUi from 'react-block-ui';
import { Card, Col, Row } from 'reactstrap';
import 'react-block-ui/style.css';
import { inject, observer } from 'mobx-react';
import MainTable from '../../components/TableMainComponent';
import { toJS } from 'mobx'
import ClearGreenButton from "../../components/GreenButton";


const styles = () => ({
    family: {
        fontFamily: 'Muli',
    },
    weight: {
        fontWeight: 'bold',
    },
    root: {
        flexGrow: 1,
    },
    tabsRoot: {
        borderBottom: '1px solid #e8e8e8',
    },
    tabsIndicator: {
        backgroundColor: '#22216E',
        fontWeight: 'bold',
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: 72,
        fontFamily: ['Muli'],
        '&:hover': {
            color: '#22216E',
            opacity: 1,
        },
        '&$tabSelected': {
            color: '#22216E',
            fontWeight: 'bold',
        },
        '&:focus': {
            color: '#22216E',
            outline: '0 !important',
            fontWeight: 'bold',
        },
    },
    tabSelected: {},
    fixed: {
        overflowX: 'scroll',
    },
});

class Order extends Component {
    componentDidMount = async () => {
        const { OrderStore, history } = this.props;
        const { getOrders } = OrderStore;
        await getOrders()
    }

    goToAddOrder = (history) => {
        history.push("/create/order");
    }

    renderOrderList() {
        const { OrderStore, history } = this.props;
        const { resultDetails, sorting, getLoading, disabledColumn, orderArray } = OrderStore;
        const columns = [
            { name: 'id', title: 'ID' },
            { name: 'userID', title: 'User ID' },
            { name: 'phoneNumber', title: 'Phone Number' },
            { name: 'shippingAddress', title: 'Shipping Address' },
            { name: 'status', title: 'Status' },
            { name: 'totalAmount', title: 'Total Amount' },
            { name: 'orderDate', title: 'Order Date' },
        ];

        if (getLoading) {
            return (
                <Grid rows={[]} columns={columns}>
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                        <LoadingCircular />
                    </div>
                </Grid>
            );
        }

        return (
            <div>
                <BlockUi
                    blocking={getLoading}
                    loader={
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <LoadingCircular />
                        </div>
                    }
                >
                    <Card className="card-shadow" style={{ marginBottom: "10px", padding: "20px", position: "relative" }}>
                        <Row>
                            <Col style={{ width: "100%", display: "flex", justifyContent: "space-between" }}>
                                <span style={{ fontFamily: "Quicksand", fontWeight: "bold", fontSize: "30px" }}>Order List</span>
                                <ClearGreenButton
                                    buttonText={"Create Order"}
                                    buttonWidth="100px"
                                    buttonHeight="40px"
                                    onClick={() => this.goToAddOrder(history)}
                                />
                            </Col>
                        </Row>
                    </Card>
                    <Card className="card-shadow">
                        <Row>
                            <Col>
                                <Row>
                                    <Col>
                                        <MainTable
                                            tableArray={toJS(orderArray) || []}
                                            cols={columns}
                                            sortingState={[]}
                                            pageSize={1000}
                                            dataCount={1000}
                                            loadingBool={getLoading}
                                            canClickRow={true}
                                            history={history}
                                            pageName={'orders'}
                                        />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Card>
                </BlockUi>
            </div>

        );
    }

    render() {
        return (
            <div>
                <p>Hello Admin</p>
                {this.renderOrderList()}
            </div>
        )
    }
}

export default withStyles(styles)(
    inject('OrderStore')(
        observer(Order),
    ),
);