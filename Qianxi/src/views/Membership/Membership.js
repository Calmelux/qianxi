import React, { Component } from "react"
import { inject, observer } from 'mobx-react';
import { toJS } from 'mobx'
import { Grid } from '@devexpress/dx-react-grid-material-ui';
import { Card, Col, Row } from 'reactstrap';
import { SortingState, IntegratedSorting, IntegratedFiltering } from '@devexpress/dx-react-grid';
import LoadingCircular from '../../components/LoadingCircular';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import MainTable from '../../components/TableMainComponent';
import ClearGreenButton from "../../components/GreenButton";



class Membership extends Component {
    componentDidMount = async () => {
        const { MembershipStore } = this.props;
        const { getUsers } = MembershipStore;
        await getUsers({});
    }

    goToAddMember = (history) => {
        history.push("/create/user");
    }

    renderUserList(history) {
        const { MembershipStore } = this.props;
        const { userArray, sorting, getLoading, disabledColumn } = MembershipStore;
        const columns = [
            { name: 'id', title: 'ID' },
            { name: 'name', title: 'Name' },
            { name: 'phoneNumber', title: 'Phone' },
            { name: 'address', title: 'Address' },
            { name: 'email', title: 'Email' },
        ];

        if (getLoading) {
            return (
                <Grid rows={[]} columns={columns}>
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                        <LoadingCircular />
                    </div>
                </Grid>
            );
        }

        return (
            <div>
                <BlockUi
                    blocking={getLoading}
                    loader={
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <LoadingCircular />
                        </div>
                    }
                >
                    <Card className="card-shadow" style={{ marginBottom: "10px", padding: "20px", position: "relative" }}>
                        <Row>
                            <Col style={{ width: "100%", display: "flex", justifyContent: "space-between" }}>
                                <span style={{ fontFamily: "Quicksand", fontWeight: "bold", fontSize: "30px" }}>Member List</span>
                                <ClearGreenButton
                                    buttonText={"Create User"}
                                    buttonWidth="100px"
                                    buttonHeight="40px"
                                    onClick={() => this.goToAddMember(history)}
                                />
                            </Col>
                        </Row>
                    </Card>
                    <Card className="card-shadow">
                        <Row>
                            <Col>
                                <Row>
                                    <Col>
                                        <MainTable
                                            tableArray={toJS(userArray) || []}
                                            cols={columns}
                                            sortingState={[]}
                                            pageSize={1000}
                                            dataCount={1000}
                                            loadingBool={getLoading}
                                            canClickRow={true}
                                            history={history}
                                            pageName={'member'}
                                        />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Card>
                </BlockUi>
            </div>
        );
    }

    render() {
        const { MembershipStore, history } = this.props;
        const { userArray } = MembershipStore;
        return (
            <div>
                <p style={{ fontFamily: "Quicksand", fontSize: "20px", fontWeight: "bold" }}>Hello, Admin</p>
                {this.renderUserList(history)}
            </div>
        )
    }
}

export default inject('MembershipStore')(
    observer(Membership),

);