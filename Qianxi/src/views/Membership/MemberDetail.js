import React, { Component } from "react"
import { inject, observer } from 'mobx-react';
import { toJS } from 'mobx'
import { Card, Col, Row } from 'reactstrap';
import TxtField from "../../components/TxtField"
import RedButton from "../../components/RedButton"
import GreenButton from "../../components/GreenButton";
import LoadingCircular from '../../components/LoadingCircular';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

class MemberDetails extends Component {

    constructor() {
        super();
        this.state = {
            changePass: true
        }
    }

    componentDidMount = async () => {
        const { MembershipStore } = this.props;
        const { getUserById } = MembershipStore;
        const { userId } = this.props.match.params;
        this.setState({ changePass: true });
        await getUserById({ userId })
    }

    componentWillUnmount = async () => {
        const { MembershipStore } = this.props;
        const { clearData } = MembershipStore;
        clearData();
    }


    handleChange = event => {
        const { MembershipStore } = this.props;
        const { editedData, setEditedData } = MembershipStore;
        let value = event.target.value;
        let { name } = event.target;

        setEditedData({
            [name]: value
        })
    }

    updateUser = async () => {
        const { MembershipStore, history } = this.props;
        const { updateUser } = MembershipStore;
        const { userId } = this.props.match.params;
        await updateUser({ userId, history });
    }

    render() {
        const { MembershipStore } = this.props;
        const { editedData, getLoading } = MembershipStore;

        return (
            <React.Fragment>
                <BlockUi
                    blocking={getLoading}
                    loader={
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <LoadingCircular />
                        </div>
                    }
                >
                    <Card className="card-shadow" style={{ padding: "5px", marginBottom: "15px" }}>
                        <Row>
                            <Col>
                                <h2 className="default-font">User Details #{editedData.id}</h2>
                            </Col>
                        </Row>
                    </Card>
                    <Card className="card-shadow">
                        <Row  >
                            <Col sm="12" md="8" lg="8" style={{ position: "relative" }}>
                                <TxtField
                                    txtName={"name"}
                                    txtPlaceholder={"Name"}
                                    txtLabel={"Name"}
                                    txtValue={editedData.name}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}

                                />
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%" }}>
                                <TxtField
                                    txtName={"email"}
                                    txtPlaceholder={"Email"}
                                    txtLabel={"Email"}
                                    txtValue={editedData.email}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                />
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%", display: "flex", flexDirection: "column" }}>
                                <TxtField
                                    txtName={"password"}
                                    txtPlaceholder={"Password"}
                                    txtLabel={"Password"}
                                    txtValue={editedData.password}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                    disableTxtField={this.state.changePass}
                                />
                                <a className="change-pass-btn" onClick={() => this.setState({ changePass: false })}>Change Password</a>
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%" }}>
                                <TxtField
                                    txtName={"phoneNumber"}
                                    txtPlaceholder={"Phone Number"}
                                    txtLabel={"Phone Number"}
                                    txtValue={editedData.phoneNumber}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                />
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%" }}>
                                <TxtField
                                    txtName={"address"}
                                    txtPlaceholder={"Address"}
                                    txtLabel={"Address"}
                                    txtValue={editedData.address}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                />
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%" }}>
                                <TxtField
                                    txtName={"facebookId"}
                                    txtPlaceholder={"Facebook ID"}
                                    txtLabel={"Facebook ID"}
                                    txtValue={editedData.password}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                    disableTxtField={true}
                                />
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%" }}>
                                <TxtField
                                    txtName={"facebookName"}
                                    txtPlaceholder={"Facebook Name"}
                                    txtLabel={"Facebook Name"}
                                    txtValue={editedData.password}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                    disableTxtField={true}
                                />
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%" }}>
                                <TxtField
                                    txtName={"facebookToken"}
                                    txtPlaceholder={"Facebook Token"}
                                    txtLabel={"Facebook Token"}
                                    txtValue={editedData.password}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                    disableTxtField={true}
                                />
                            </Col>
                        </Row>
                        <Row className="flex-row">
                            <Col style={{ margin: "15px" }}>
                                <GreenButton
                                    buttonText={"Save"}
                                    onClick={() => this.updateUser()}
                                    isDisabled={false}
                                    buttonWidth="100px"
                                    buttonHeight="40px"
                                />
                            </Col>
                            <Col style={{ margin: "15px" }}>
                                <RedButton
                                    buttonText={"Ban User"}
                                    onClick={() => console.log("Ban!")}
                                    isDisabled={false}
                                    buttonWidth="100px"
                                    buttonHeight="40px"
                                />
                            </Col>
                        </Row>
                    </Card>
                </BlockUi>
            </React.Fragment>
        )
    }
}
export default inject('MembershipStore')(
    observer(MemberDetails),
);