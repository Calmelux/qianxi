import React, { Component } from 'react';
import { Card, Col, Row } from 'reactstrap';
import { inject, observer } from 'mobx-react';
import { Grid } from '@devexpress/dx-react-grid-material-ui';
import LoadingCircular from '../../components/LoadingCircular';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import MainTable from '../../components/TableMainComponent';
import ClearGreenButton from "../../components/GreenButton";
import { toJS } from 'mobx'
class Categories extends Component {

    componentDidMount = async () => {
        const { ProductStore } = this.props;
        const { getProducts } = ProductStore;
        await getProducts();
    }

    goToAddProduct = (history) => {
        history.push("/create/product");
    }

    renderProductList(history) {
        const { ProductStore } = this.props;
        const { productArray, sorting, getLoading, disabledColumn } = ProductStore;
        console.log("productArray---", toJS(productArray))
        const columns = [
            { name: 'id', title: 'ID' },
            { name: 'productId', title: 'Product ID' },
            { name: 'name', title: 'Name' },
            { name: 'price', title: 'Price' },
            { name: 'stock', title: 'Stock' },
        ];
        if (getLoading) {
            return (
                <Grid rows={[]} columns={columns}>
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                        <LoadingCircular />
                    </div>
                </Grid>
            );
        }

        return (
            <div>
                <BlockUi
                    blocking={getLoading}
                    loader={
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <LoadingCircular />
                        </div>
                    }
                >
                    <Card className="card-shadow" style={{ marginBottom: "10px", padding: "20px", position: "relative" }}>
                        <Row>
                            <Col style={{ width: "100%", display: "flex", justifyContent: "space-between" }}>
                                <span style={{ fontFamily: "Quicksand", fontWeight: "bold", fontSize: "30px" }}>Product List</span>
                                <ClearGreenButton
                                    buttonText={"Create Product"}
                                    buttonWidth="100px"
                                    buttonHeight="40px"
                                    onClick={() => this.goToAddProduct(history)}
                                />
                            </Col>
                        </Row>
                    </Card>
                    <Card className="card-shadow">
                        <Row>
                            <Col>
                                <Row>
                                    <Col>
                                        <MainTable
                                            tableArray={toJS(productArray) || []}
                                            cols={columns}
                                            sortingState={[]}
                                            pageSize={1000}
                                            dataCount={1000}
                                            loadingBool={getLoading}
                                            canClickRow={true}
                                            history={history}
                                            pageName={'products'}
                                        />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Card>
                </BlockUi>
            </div>
        );
    }

    render() {
        const { ProductStore, history } = this.props;
        const { userArray } = ProductStore;
        return (
            <div>
                <p style={{ fontFamily: "Quicksand", fontSize: "20px", fontWeight: "bold" }}>Hello, Admin</p>
                {this.renderProductList(history)}
            </div>
        )
    }
}

export default inject('ProductStore')(observer(Categories));
