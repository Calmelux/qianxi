import React, { Component } from "react"
import { inject, observer } from 'mobx-react';
import { toJS } from 'mobx'
import { Card, Col, Row } from 'reactstrap';
import TxtField from "../../components/TxtField"
import RedButton from "../../components/RedButton"
import GreenButton from "../../components/GreenButton";
import LoadingCircular from '../../components/LoadingCircular';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

class CreateProduct extends Component {
    componentDidMount = async () => {
        const { ProductStore } = this.props;
        const { clearData } = ProductStore;
        clearData();
    }

    componentWillUnmount = async () => {
        const { ProductStore } = this.props;
        const { clearData } = ProductStore;
        clearData();
    }


    handleChange = event => {
        const { ProductStore } = this.props;
        const { setEditedData } = ProductStore;
        let value = event.target.value;
        let { name } = event.target;

        setEditedData({
            [name]: value
        })
    }

    createProduct = async () => {
        const { ProductStore, history } = this.props;
        const { createProduct } = ProductStore;
        await createProduct({ history });
    }

    render() {
        const { ProductStore } = this.props;
        const { editedData, getLoading } = ProductStore;

        return (
            <React.Fragment>
                <BlockUi
                    blocking={getLoading}
                    loader={
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <LoadingCircular />
                        </div>
                    }
                >
                    <Card className="card-shadow" style={{ padding: "5px", marginBottom: "15px" }}>
                        <Row>
                            <Col>
                                <h2 className="default-font">Create Product</h2>
                            </Col>
                        </Row>
                    </Card>
                    <Card className="card-shadow">
                        <Row  >
                            <Col sm="12" md="8" lg="8" style={{ position: "relative" }}>
                                <TxtField
                                    txtName={"name"}
                                    txtPlaceholder={"Name"}
                                    txtLabel={"Name"}
                                    txtValue={editedData.name}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}

                                />
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%" }}>
                                {/* <TxtField
                                    txtName={"image"}
                                    txtPlaceholder={"Image"}
                                    txtLabel={"Image"}
                                    txtValue={editedData.email}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                /> */}
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%", display: "flex", flexDirection: "column" }}>
                                <TxtField
                                    txtName={"stock"}
                                    txtPlaceholder={"Stock"}
                                    txtLabel={"Stock"}
                                    txtValue={editedData.stock + ""}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                    txtType={"number"}
                                    disableTxtField={false}
                                />
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%" }}>
                                <TxtField
                                    txtName={"price"}
                                    txtPlaceholder={"Price"}
                                    txtLabel={"Price"}
                                    txtValue={editedData.price + ""}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    txtType={"number"}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                />
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%" }}>
                                <TxtField
                                    txtName={"description"}
                                    txtPlaceholder={"Description"}
                                    txtLabel={"Description"}
                                    txtValue={editedData.description + ""}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                />
                            </Col>
                            <Col sm="12" md="8" lg="8" style={{ width: "100%" }}>
                                <TxtField
                                    txtName={"categoryId"}
                                    txtPlaceholder={"Category ID"}
                                    txtLabel={"Category ID"}
                                    txtValue={editedData.categoryId}
                                    handleChange={this.handleChange}
                                    txtMarginLeft={15}
                                    txtMarginRight={15}
                                    txtMarginBottom={15}
                                    fontFamily={"Quicksand"}
                                    txtWidth={"50%"}
                                    disableTxtField={false}
                                />
                            </Col>
                        </Row>
                        <Row className="flex-row">
                            <Col style={{ margin: "15px" }}>
                                <GreenButton
                                    buttonText={"Save"}
                                    onClick={() => this.createProduct()}
                                    isDisabled={false}
                                    buttonWidth="100px"
                                    buttonHeight="40px"
                                />
                            </Col>
                        </Row>
                    </Card>
                </BlockUi>
            </React.Fragment>
        )
    }
}
export default inject('ProductStore')(
    observer(CreateProduct),
);