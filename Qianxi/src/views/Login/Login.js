import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { toJS } from 'mobx';
import { Card, Col, Row } from 'reactstrap';
import TxtField from "../../components/TxtField"
import PasswordTxtField from "../../components/PasswordTxtField"
import GreenButton from "../../components/GreenButton"
import { Redirect } from 'react-router-dom';
import { getCookie } from '../../helper/cookie';


class Login extends Component {
    state = {
        showPassword: false,
    };

    handleClickShowPassword = () => {
        this.setState(state => ({ showPassword: !state.showPassword }));
    };

    _handleKeyPress = e => {
        if (e.key === 'Enter') {
            this.login();
        }
    };

    login = async () => {
        const { AuthStore, history } = this.props;
        const { login } = AuthStore;
        await login(history)
    }

    handleChange = event => {
        const { AuthStore } = this.props;
        const { setCredentials } = AuthStore;
        let { name, value } = event.target;

        setCredentials({
            [name]: value
        })
    }

    render() {
        const { AuthStore } = this.props;
        const { credentials } = AuthStore;
        let token = getCookie('token');

        if (token) {
            return <Redirect to="/member" />
        }

        return (
            <div style={{ backgroundColor: "lightgrey", height: "100vh", width: "100vw" }}>
                <div style={{ display: "flex", height: "100vh", width: "100vw", justifyContent: "center", alignItems: "center" }}>
                    <Card className="card-shadow login-card" style={{ position: "relative", padding: "40px" }}>
                        <Row>
                            <Col style={{ position: "relative", fontFamily: "Quicksand" }}>
                                <h1>Please Login,</h1>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm="12" md="12" lg="12" style={{ position: "relative", fontFamily: "Quicksand", width: "100%" }}>
                                <TxtField
                                    txtName="email"
                                    txtLabel="Email"
                                    txtPlaceholder="Email"
                                    txtWidth='100%'
                                    handleChange={this.handleChange}
                                />
                            </Col>
                            <Col sm="12" md="12" lg="12" style={{ position: "relative", fontFamily: "Quicksand", width: "100%" }}>
                                <PasswordTxtField
                                    txtName="password"
                                    txtLabel="Password"
                                    txtPlaceholder="Password"
                                    txtWidth='100%'
                                    handleClickShowPassword={this.handleClickShowPassword}
                                    showPassword={this.state.showPassword}
                                    handleIconKeyPress={this.handleClickShowPassword}
                                    handleKeyPress={this._handleKeyPress}
                                    txtFieldType={this.state.showPassword ? 'text' : 'password'}
                                    handleChange={this.handleChange}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{ position: "relative", fontFamily: "Quicksand" }}>
                                {credentials && credentials.email === "" ?
                                    <div style={{ color: "red" }}>
                                        <p>Please enter your email</p>
                                    </div> : credentials.password === "" ?
                                        <div style={{ color: "red" }}>
                                            <p>Please enter your password</p>
                                        </div> : <div />
                                }
                            </Col>
                        </Row>
                        <Row style={{ paddingTop: "50%" }}>
                            <Col style={{ position: "relative", fontFamily: "Quicksand" }}>
                                <GreenButton
                                    buttonWidth="100%"
                                    buttonHeight="50px"
                                    buttonText="Login"
                                    onClick={() => this.login()}
                                />
                            </Col>
                        </Row>
                    </Card>
                </div>
            </div>
        )
    }
}
export default inject('AuthStore')(observer(Login));