import React, { Component, Fragment } from "react";
import { Provider } from "mobx-react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from './views/Login/Login';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import createGenerateClassName from "@material-ui/styles/createGenerateClassName"


import "./scss/style.css";
import JssProvider from 'react-jss/lib/JssProvider';

import { DefaultLayout } from "./containers";

import { Page404 } from "./views/Pages";

import stores from "./stores";

import 'font-awesome/css/font-awesome.min.css';
const generateClassName = createGenerateClassName({
  dangerouslyUseGlobalCSS: true,
});

class App extends Component {

  render() {
    return (
      <JssProvider generateClassName={generateClassName}>
        <MuiThemeProvider theme={createMuiTheme()}>
          <Provider {...stores}>
            <Fragment>
              <BrowserRouter>
                <Switch>
                  <Route exact path="/login" name="Login Page" component={Login} />
                  <Route exact path="/404" name="Page 404" component={Page404} />
                  <Route path="/" name="Home" component={DefaultLayout} />
                </Switch>
              </BrowserRouter>
            </Fragment>
          </Provider>
        </MuiThemeProvider>
      </JssProvider>
    );
  }
}

export default App;
