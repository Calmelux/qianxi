import React, { Component } from 'react';
import { TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  textField: {
    fontFamily: ['Muli'],
    fontSize: 14,
  },
  labelRoot: {
    fontFamily: ['Muli'],
    fontSize: 14,
    lineHeight: 0.7,
  },
  rootHeight: {
    height: 48,
  },
};

class TxtField extends Component {
  state = {
    marginTop: this.props.txtMarginTop,
    marginRight: this.props.txtMarginRight,
    marginBottom: this.props.txtMarginBottom,
    marginLeft: this.props.txtMarginLeft,

    height: this.props.txtHeight,
    width: this.props.txtWidth,
    fontFamily: this.props.fontFamily
  };

  render() {
    const { classes } = this.props;

    return (
      <TextField
        variant="outlined"
        className="w-100"
        style={{
          marginTop: this.state.marginTop,
          marginRight: this.state.marginRight,
          marginBottom: this.state.marginBottom,
          marginLeft: this.state.marginLeft,

          height: this.state.height,
          width: this.state.width,
          fontFamily: "Quicksand"
        }}
        id={this.props.txtId}
        type={this.props.txtType}
        label={this.props.txtLabel}
        name={this.props.txtName}
        placeholder={this.props.txtPlaceholder}
        value={this.props.txtValue}
        onChange={this.props.handleChange}
        margin="normal"
        onKeyPress={this.props.handleKeyPress}
        onFocus={this.props.onFocus}
        onBlur={this.props.onBlur}
        InputProps={{
          classes: {
            root: classes.rootHeight,
            input: classes.textField,
          },
          inputProps: { min: 0, max: this.props.maxInput },
        }}
        InputLabelProps={{
          FormLabelClasses: {
            root: classes.labelRoot,
          },
        }}
        disabled={this.props.disableTxtField}
      />
    );
  }
}

export default withStyles(styles)(TxtField);
