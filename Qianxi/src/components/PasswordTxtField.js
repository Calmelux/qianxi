import React, { Component } from "react";
import { TextField } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

const styles = {
  textField: {
    fontFamily: ["Muli"],
    fontSize: 14
  },
  labelRoot: {
    fontFamily: ["Muli"],
    fontSize: 14,
    lineHeight: 0.7
  },
  rootHeight: {
    height: 48
  }
};

class TextFieldComponent extends Component {
  state = {
    marginTop: this.props.txtMarginTop,
    marginRight: this.props.txtMarginRight,
    marginBottom: this.props.txtMarginBottom,
    marginLeft: this.props.txtMarginLeft,

    height: this.props.txtHeight,
    width: this.props.txtWidth,

    id: this.props.txtId,
    label: this.props.txtLabel,
    name: this.props.txtName,
    placeholder: this.props.txtPlaceholder,
    value: this.props.txtValue,
    disabled: this.props.disableTxtField
  };

  render() {
    const { classes } = this.props;
    return (
      <TextField
        className="w-100"
        variant="outlined"
        style={{
          marginTop: this.state.marginTop,
          marginRight: this.state.marginRight,
          marginBottom: this.state.marginBottom,
          marginLeft: this.state.marginLeft,
          width: this.state.width,
          fontFamily: "Quicksand"
        }}
        label={this.props.txtLabel}
        name={this.props.txtName}
        value={this.props.txtValue}
        id={this.props.txtid}
        placeholder={this.props.txtPlaceholder}
        type={this.props.txtFieldType}
        onChange={this.props.handleChange}
        onKeyPress={this.props.handleKeyPress}
        margin={"normal"}
        InputProps={{
          classes: {
            root: classes.rootHeight,
            input: classes.textField
          },
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                aria-label="Toggle password visibility"
                onClick={this.props.handleClickShowPassword}
                onKeyPress={!this.props.handleIconKeyPress}
              >
                {this.props.showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          )
        }}
        InputLabelProps={{
          FormLabelClasses: {
            root: classes.labelRoot
          }
        }}
        disabled={this.props.disableTxtField}
      />
    );
  }
}

export default withStyles(styles)(TextFieldComponent);
