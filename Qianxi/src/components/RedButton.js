import React, { Component } from 'react';
import { Button } from 'reactstrap';

class ClearRedButton extends Component {
  state = {
    marginTop: this.props.buttonMarginTop,
    marginRight: this.props.buttonMarginRight,
    marginBottom: this.props.buttonMarginBottom,
    marginLeft: this.props.buttonMarginLeft,

    height: this.props.buttonHeight,
    width: this.props.buttonWidth,

    text: this.props.buttonText,
  };

  render() {
    return (
      <Button
        className={'btn-cancel'}
        style={{
          fontFamily: 'Muli',
          fontWeight: 'bold',
          height: this.state.height,
          width: this.state.width,

          marginTop: this.state.marginTop,
          marginRight: this.state.marginRight,
          marginBottom: this.state.marginBottom,
          marginLeft: this.state.marginLeft,
        }}
        id={this.props.buttonId}
        onClick={this.props.onClick}
        disabled={this.props.isDisabled}
      >
        {this.state.text}
      </Button>
    );
  }
}
export default ClearRedButton;
