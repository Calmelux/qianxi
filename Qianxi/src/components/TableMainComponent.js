import React, { Component } from 'react';
import LoadingCircular from './LoadingCircular';
import {
  PagingState,
  CustomPaging,
  SortingState,
  IntegratedFiltering,
  IntegratedSorting,
} from '@devexpress/dx-react-grid';
import { Grid, PagingPanel, TableHeaderRow, Table, TableColumnResizing } from '@devexpress/dx-react-grid-material-ui';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
  family: {
    fontFamily: 'Muli',
  },
  weight: {
    fontWeight: 'bold',
  },
});

const TableComponentBase = ({ classes, ...restProps }) => <Table.Table {...restProps} className={classes.family} />;
export const TableComponent = withStyles(styles, { name: 'TableComponent' })(TableComponentBase);

const TableHeaderComponentBase = ({ classes, ...restProps }) => (
  <TableHeaderRow.Cell {...restProps} className={classes.weight} />
);
export const TableHeaderComponent = withStyles(styles)(TableHeaderComponentBase);

class TableMainComponent extends Component {
  Cell = props => {
    const { row, column } = props;
    const { history } = this.props;
    let { valueTransformer } = column;
    if (valueTransformer) {
      return (
        <Table.Cell>
          <div>{row ? valueTransformer(row, history) : ''}</div>
        </Table.Cell>
      );
    }
    return <Table.Cell {...props} />;
  };

  onRowClicked = ({ row }) => {
    const { history, pageName } = this.props;
    const organizationId = localStorage.getItem('currentOrganization');
    const isSuperAdmin = localStorage.getItem('isSuperAdmin');
    if (isSuperAdmin && isSuperAdmin == 'true') {
      this.props.history.push(`/${pageName}/${row.id}`);
    } else {
      this.props.history.push(`/${pageName}/${row.id}`);
    }
  };

  tableRow = ({ row, ...restProps }) => {
    return (
      <Table.Row
        {...restProps}
        style={{ cursor: 'pointer' }}
        onClick={() => this.onRowClicked({ row })}
        className="cursor-pointer table-row toggle-hover"
      />
    );
  };

  tableRowNoClick = ({ row, ...restProps }) => {
    return <Table.Row {...restProps} className="cursor-pointer table-row" />;
  };

  render() {
    const {
      tableArray,
      cols,
      columnWidths,
      sortingState,
      disabledCols,
      handleSortChange,
      currentPage,
      changeCurrentPage,
      pageSize,
      dataCount,
      loadingBool,
      canClickRow,
      tableColumnExtensions,
    } = this.props;
    if (loadingBool) {
      return (
        <Grid rows={[]} columns={cols}>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <LoadingCircular />
          </div>
        </Grid>
      );
    }
    return (
      <div style={{ padding: '15px' }}>
        <Grid rows={tableArray} columns={cols}>
          {sortingState.length > 0 && (
            <React.Fragment>
              <SortingState sorting={sortingState} onSortingChange={handleSortChange} columnExtensions={disabledCols} />
              {/* <IntegratedSorting /> */}
              <IntegratedFiltering />
            </React.Fragment>
          )}

          {/*current page -1 means it doesn't have paging*/}
          {currentPage > -1 && (
            <React.Fragment>
              <PagingState currentPage={currentPage} onCurrentPageChange={changeCurrentPage} pageSize={pageSize} />
              <CustomPaging totalCount={dataCount} />
              <PagingPanel />
            </React.Fragment>
          )}

          {canClickRow ? (
            <Table tableComponent={TableComponent} cellComponent={this.Cell} rowComponent={this.tableRow} />
          ) : (
              <Table
                tableComponent={TableComponent}
                cellComponent={this.Cell}
                rowComponent={this.tableRowNoClick}
                columnExtensions={tableColumnExtensions}
              />
            )}
          {columnWidths && <TableColumnResizing defaultColumnWidths={columnWidths} />}
          <TableHeaderRow
            showSortingControls={sortingState.length > 0 ? true : false}
            cellComponent={TableHeaderComponent}
          />
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(TableMainComponent);
