import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import BrowseButton from './GreenButton';
import classNames from 'classnames';
import { toJS } from 'mobx';

class DropzoneImage extends Component {
    deleteImg = () => {
        const { store } = this.props;
        const imageUrl = store.imageUrl;
        store.setEditedData({
            imageUrl: '',
        });
    };

    validateRes = async (reader, store) => {

        reader.onload = async function (file) {
            let image = new Image();
            image.src = file.target.result;
            let base64 = image.src;

            store.setEditedData({
                imageUrl: base64
            })
        };
    };

    handleDrop = async (accepted, rejected, files) => {
        const { store } = this.props;

        const file = accepted[0];
        let reader = new FileReader();
        await this.validateRes(reader, store);

        if (accepted.length > 0) {
            reader.onloadend = async () => {
                await store.setEditedData({
                    files: accepted,
                    imageUrl: reader.result,
                });
            };
            await reader.readAsDataURL(file);
        }
    };

    render() {
        const { store } = this.props;
        const imageUrl = store.editedData.imageUrl;
        const { multiple, sizeFormatText, title, currentImage } = this.props;

        return (
            <React.Fragment>
                {!imageUrl ? (
                    <Dropzone onDrop={this.handleDrop} multiple={multiple}>
                        {({ getRootProps, getInputProps, isDragActive }) => {
                            return (
                                <div
                                    {...getRootProps()}
                                    className={classNames('dropzone', {
                                        'dropzone--isActive': isDragActive,
                                    })}
                                    style={{
                                        border: '1px dashed #A1AEB7',
                                        borderRadius: '6px',
                                        padding: '50px',
                                        width: '100%',
                                        display: 'flex',
                                        justifyContent: 'center',
                                    }}
                                >
                                    <input {...getInputProps()} />
                                    {isDragActive ? (
                                        <p />
                                    ) : (
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    flexDirection: 'column',
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    width: '100%',
                                                }}
                                            >
                                                <span
                                                    style={{
                                                        fontFamily: 'Muli',
                                                        paddingBottom: '10px',
                                                    }}
                                                >
                                                    {title}
                                                </span>
                                                <BrowseButton isBlock={true} buttonHeight={'36px'} buttonText={'Browse'} buttonMarginLeft={'0'} />
                                                <span
                                                    style={{
                                                        fontFamily: 'Muli',
                                                        paddingTop: '10px',
                                                    }}
                                                >
                                                    {sizeFormatText}
                                                </span>
                                            </div>
                                        )}
                                </div>
                            );
                        }}
                    </Dropzone>
                ) : (
                        <div
                            style={{
                                width: '100%',
                                maxWidth: '520px',
                                position: 'relative',
                            }}
                        >
                            <img
                                src={imageUrl}
                                alt=" "
                                responsive="true"
                                style={{
                                    width: '100%',
                                    height: '100%',
                                    maxHeight: '277px',
                                    maxWidth: '555px',
                                }}
                            />
                            <div
                                onClick={this.deleteImg}
                                style={{
                                    width: 'fit-content',
                                    position: 'absolute',
                                    bottom: 5,
                                    right: 5,
                                }}
                            >
                                <i className="fa fa-minus-square" style={{ color: 'red' }} />
                            </div>
                        </div>
                    )}
            </React.Fragment>
        );
    }
}

export default DropzoneImage;
