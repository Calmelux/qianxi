import axios from "../helper/Axios"
import { adminUrl } from "../config/config"

export async function GetOrders({ }) {
    try {
        let orderResponse = await axios.Get({
            url: `${adminUrl}/orders`
        });
        if (orderResponse.statusCode !== 200) {
            return { error: orderResponse.message };
        }
        return { data: orderResponse.data };
    } catch (e) {
        if (e.response) {
            return { error: e.response };
        } else {
            return { error: e };
        }
    }
}

export async function GetOrdersById({ orderId }) {
    try {
        let orderResponse = await axios.Get({
            url: `${adminUrl}/orders/${orderId}`
        });
        if (orderResponse.statusCode !== 200) {
            return { error: orderResponse.message };
        }
        return { data: orderResponse.data };
    } catch (e) {
        if (e.response) {
            return { error: e.response };
        } else {
            return { error: e };
        }
    }
}


export async function CreateOrder({ name, permission }) {
    try {
        let roleResponse = await axios.Post({
            url: `${adminUrl}/orders/create`,
            data: { name, permission }
        });
        if (roleResponse.statusCode !== 200) {
            return { error: roleResponse.message };
        }
        return { data: roleResponse.data };
    } catch (error) {
        if (error.response) {
            return { error: error.response };
        } else {
            return { error: error };
        }
    }
}


export async function UpdateOrder({ orderId, name, permission }) {
    try {
        let roleResponse = await axios.Put({
            url: `${adminUrl}/orders/${orderId}`,
            data: { name, permission }
        });
        if (roleResponse.statusCode !== 200) {
            return { error: roleResponse.message };
        }
        return { data: roleResponse.data };
    } catch (error) {
        if (error.response) {
            return { error: error.response };
        } else {
            return { error: error };
        }
    }
}