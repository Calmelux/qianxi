import axios from "../helper/Axios"
import { adminUrl } from "../config/config"

export async function GetUsers({ }) {
    try {
        let userResponse = await axios.Get({
            url: `${adminUrl}/users`
        });
        if (userResponse.statusCode !== 200) {
            return { error: userResponse.message };
        }
        return { data: userResponse.data };
    } catch (e) {
        if (e.response) {
            return { error: e.response };
        } else {
            return { error: e };
        }
    }
}

export async function GetUserById({ userId }) {
    try {
        let userResponse = await axios.Get({
            url: `${adminUrl}/users/${userId}`
        });
        if (userResponse.statusCode !== 200) {
            return { error: userResponse.message };
        }
        return { data: userResponse.data };
    } catch (e) {
        if (e.response) {
            return { error: e.response };
        } else {
            return { error: e };
        }
    }
}


export async function CreateUser({ name, email, password, phoneNumber, address, facebookId, facebookName, facebookToken }) {
    try {
        let userResponse = await axios.Post({
            url: `${adminUrl}/users/create`,
            data: { name, email, password, phoneNumber, address, facebookId, facebookName, facebookToken }
        });
        if (userResponse.statusCode !== 200) {
            return { error: userResponse.message };
        }
        return { data: userResponse.data };
    } catch (error) {
        if (error.response) {
            return { error: error.response };
        } else {
            return { error: error };
        }
    }
}


export async function UpdateUser({ userId, name, email, password, isActive, creditAmount }) {
    try {
        let userResponse = await axios.Put({
            url: `${adminUrl}/users/${userId}`,
            data: { name, email, password, isActive, creditAmount }
        });
        if (userResponse.statusCode !== 200) {
            return { error: userResponse.message };
        }
        return { data: userResponse.data };
    } catch (error) {
        if (error.response) {
            return { error: error.response };
        } else {
            return { error: error };
        }
    }
}


export async function BanUser({ userId }) {
    try {
        let userResponse = await axios.Delete({
            url: `${adminUrl}/organizations/${userId}`
        });
        if (userResponse.statusCode !== 200) {
            return { errorusers: userResponse.message };
        }
        return { datausers: userResponse.data };
    } catch (e) {
        if (e.response) {
            return { erroruser: e.response };
        } else {
            return { erroruser: e };
        }
    }
}