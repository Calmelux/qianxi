import axios from "../helper/Axios"
import { adminUrl } from "../config/config"


export async function Login({ email, password }) {
    try {
        let roleResponse = await axios.Post({
            url: `${adminUrl}/auth/login`,
            data: { email, password }
        });
        if (roleResponse.statusCode !== 200) {
            return { error: roleResponse.message };
        }
        return { data: roleResponse.data };
    } catch (error) {
        if (error.response) {
            return { error: error.response };
        } else {
            return { error: error };
        }
    }
}