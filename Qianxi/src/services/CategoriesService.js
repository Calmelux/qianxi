import axios from "../helper/Axios"
import { adminUrl } from "../config/config"

export async function GetCategories({ }) {
    try {
        let categoryResponse = await axios.Get({
            url: `${adminUrl}/categories`
        });
        if (categoryResponse.statusCode !== 200) {
            return { error: categoryResponse.message };
        }
        return { data: categoryResponse.data };
    } catch (e) {
        if (e.response) {
            return { error: e.response };
        } else {
            return { error: e };
        }
    }
}

export async function GetCategoryById({ categoryId }) {
    try {
        let categoryResponse = await axios.Get({
            url: `${adminUrl}/categories/${categoryId}`
        });
        if (categoryResponse.statusCode !== 200) {
            return { error: categoryResponse.message };
        }
        return { data: categoryResponse.data };
    } catch (e) {
        if (e.response) {
            return { error: e.response };
        } else {
            return { error: e };
        }
    }
}

export async function CreateCategory({ name }) {
    try {
        let categoryResponse = await axios.Post({
            url: `${adminUrl}/categories/create`,
            data: { name }
        });
        if (categoryResponse.statusCode !== 200) {
            return { error: categoryResponse.message };
        }
        return { data: categoryResponse.data };
    } catch (error) {
        if (error.response) {
            return { error: error.response };
        } else {
            return { error: error };
        }
    }
}

export async function UpdateCategory({ categoryId, name }) {
    try {
        let categoryResponse = await axios.Put({
            url: `${adminUrl}/categories/${categoryId}`,
            data: { name }
        });
        if (categoryResponse.statusCode !== 200) {
            return { error: categoryResponse.message };
        }
        return { data: categoryResponse.data };
    } catch (error) {
        if (error.response) {
            return { error: error.response };
        } else {
            return { error: error };
        }
    }
}