import axios from "../helper/Axios"
import { adminUrl } from "../config/config"

export async function GetProducts({ }) {
    try {
        let productResponse = await axios.Get({
            url: `${adminUrl}/products`
        });
        if (productResponse.statusCode !== 200) {
            return { error: productResponse.message };
        }
        return { data: productResponse.data };
    } catch (e) {
        if (e.response) {
            return { error: e.response };
        } else {
            return { error: e };
        }
    }
}

export async function GetProductById({ productId }) {
    try {
        let productResponse = await axios.Get({
            url: `${adminUrl}/products/${productId}`
        });
        if (productResponse.statusCode !== 200) {
            return { error: productResponse.message };
        }
        return { data: productResponse.data };
    } catch (e) {
        if (e.response) {
            return { error: e.response };
        } else {
            return { error: e };
        }
    }
}

export async function CreateProduct({ name, stock, description, imageUrl, price, categoryId }) {
    try {
        let roleResponse = await axios.Post({
            url: `${adminUrl}/products/create`,
            data: { name, stock, description, imageUrl, price, categoryId }
        });
        if (roleResponse.statusCode !== 200) {
            return { error: roleResponse.message };
        }
        return { data: roleResponse.data };
    } catch (error) {
        if (error.response) {
            return { error: error.response };
        } else {
            return { error: error };
        }
    }
}


export async function UpdateProduct({ productId, name, stock, description, imageUrl, price, categoryId }) {
    try {
        let roleResponse = await axios.Put({
            url: `${adminUrl}/products/${productId}`,
            data: { name, stock, description, imageUrl, price, categoryId }
        });
        if (roleResponse.statusCode !== 200) {
            return { error: roleResponse.message };
        }
        return { data: roleResponse.data };
    } catch (error) {
        if (error.response) {
            return { error: error.response };
        } else {
            return { error: error };
        }
    }
}