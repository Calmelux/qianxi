import React from "react";
import { observable, action, decorate, computed, toJS } from "mobx";
import { GetOrders, GetOrdersById, CreateOrder, UpdateOrder } from "../services/OrderService"
import swal from "sweetalert2";
import moment from 'moment'

const initialOrderData = {
    id: "",
    name: "",
    totalAmount: "",
    productList: "",
    description: "",
    productListID: "",
    orderDate: "",
    quantity: ""
}

class OrderStore {

    getLoading = false;
    editedData = initialOrderData;
    orderArray = [];

    statusConverter = (status) => {
        const statusName = status === 'waiting_payment' ? "Waiting Payment" : status === "completed" ? "Completed" : status === "cart" ? "Cart" : "-"
        return statusName;
    }

    swalModal = (title, text, icon, showCancelButton) => {
        swal.fire({
            title: title,
            icon: icon,
            text: text,
            showCancelButton: showCancelButton
        })
    }

    clearData = () => {
        this.editedData = initialOrderData;
    }

    setEditedData = updatedData => {
        this.editedData = {
            ...this.editedData,
            ...updatedData
        }
    }

    getOrders = async () => {
        this.getLoading = true;
        const { data, error } = await GetOrders({});
        this.getLoading = false;
        if (data) {
            console.log("hellol", toJS(data))
            this.orderArray = data.docs.map((items) => {
                const id = items.id || "-";
                const userId = items.userId || "-";
                const orderDate = moment(items.orderDate).format("DD MMM YYYY") || "-";
                const phoneNumber = items.phoneNumber || "-";
                const shippingAddress = items.shippingAddress || "-";
                const status = this.statusConverter(items.status) || "-";
                const totalAmount = items.totalAmount || "-";

                return { id: id, userId: userId, orderDate: orderDate, phoneNumber: phoneNumber, shippingAddress: shippingAddress, status: status, totalAmount: totalAmount }
            });
        } else {
            if (error) {
                return this.swalModal("Oops, we can't seem to fetch the data!", error, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", error, "error", false)
            }
        }
    }
    getOrderById = async ({ orderId }) => {
        this.getLoading = true;
        const { data, error } = await GetOrdersById({ orderId });
        this.getLoading = false;
        if (data) {
            this.editedData.totalAmount = data.totalAmount;
            this.editedData.productListID = data.productListID;
            this.editedData.quantity = data.quantity;
            this.editedData.orderDate = moment(data.orderDate).format("DD MMM YYYY");
        }
        else {
            if (error) {
                return this.swalModal("Oops, we can't seem to fetch current data!", error.data.message, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", error.data.message, "error", false)
            }
        }
    }


    createOrder = async ({ history }) => {
        this.getLoading = true;
        const { data, error } = await CreateOrder({
            productListID: this.editedData.productListID,
            totalAmount: this.editedData.totalAmount,
            quantity: this.editedData.quantity,
            orderDate: new Date(moment(this.editedData.orderDate).format("YYYY-MM-DD")),
            orderStatus: "waiting_payment"
        });
        this.getLoading = false;
        if (data) {
            this.swalModal("Success!", "Orider created!", "success", false)
            history.push("/orders")
        }
        else {
            if (error) {
                const message = error && error.data ? error.data.message : ""
                this.swalModal("Sorry!", message, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", "", "error", false)
            }
        }
    }

    updateOrder = async ({ orderId, history }) => {
        this.getLoading = true;
        const { data, error } = await UpdateOrder({
            orderId,
            productListID: this.editedData.productListID,
            totalAmount: this.editedData.totalAmount,
            quantity: this.editedData.quantity,
            orderDate: new Date(moment(this.editedData.orderDate).format("YYYY-MM-DD"))
        })
        this.getLoading = false;
        if (data) {
            this.swalModal("Success!", "Order updated!", "success", false)
            history.push("/orders")
        }
        else {
            if (error) {
                const message = error && error.data ? error.data.message : ""
                this.swalModal("Sorry!", message, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", "", "error", false)
            }
        }
    }
}

decorate(OrderStore, {
    getLoading: observable,
    editedData: observable,
    orderArray: observable,

    getOrders: action,
    getOrderById: action,
    createOrder: action,
    updateOrder: action,
    swalModal: action,
    setEditedData: action,
    clearData: action,
    statusConverter: action
});

export default new OrderStore();