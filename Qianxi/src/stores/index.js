import LandingStore from "./LandingStore";
import MembershipStore from "./MembershipStore";
import ProductStore from "./ProductStore";
import OrderStore from "./OrderStore";
import AuthStore from "./AuthStore";
import CategoriesStore from "./CategoriesStore";
export default {
    LandingStore,
    MembershipStore,
    ProductStore,
    OrderStore,
    AuthStore,
    CategoriesStore
}