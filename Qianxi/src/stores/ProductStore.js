import { observable, action, decorate, computed, toJS } from "mobx";
import { GetProducts, GetProductById, CreateProduct, UpdateProduct } from "../services/ProductService"
import swal from "sweetalert2";

const initialProductsData = {
    id: "",
    name: "",
    stock: "",
    price: "",
    categoryId: "",
    description: "",
}
class ProductStore {
    getLoading = false;
    editedData = initialProductsData;
    productArray = [];


    swalModal = (title, text, icon, showCancelButton) => {
        swal.fire({
            title: title,
            icon: icon,
            text: text,
            showCancelButton: showCancelButton
        })
    }

    clearData = () => {
        this.editedData = initialProductsData;
    }

    setEditedData = updatedData => {
        this.editedData = {
            ...this.editedData,
            ...updatedData
        }
    }

    getProducts = async () => {
        this.getLoading = true;
        const { data, error } = await GetProducts({});
        this.getLoading = false;
        if (data) {
            this.productArray = data.docs.map((items) => {
                const id = items.id || "-";
                const productId = items.productId || "-";
                const name = items.name || "-";
                const price = items.price || "-";
                const stock = items.stock || "-";

                return { id: id, productId: productId, name: name, price: price, stock: stock }
            });
        } else {
            if (error) {
                return this.swalModal("Oops, we can't seem to fetch the data!", error, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", error, "error", false)
            }
        }
    }

    getProductById = async ({ productId }) => {
        this.getLoading = true;
        const { data, error } = await GetProductById({ productId });
        this.getLoading = false;
        if (data) {
            console.log("Data---", toJS(data));
            this.editedData.id = data.id;
            this.editedData.name = data.name;
            this.editedData.stock = data.stock;
            this.editedData.price = data.price;
            this.editedData.categoryId = data.categoryId
            this.editedData.description = data.description
        }
        else {
            if (error) {
                return this.swalModal("Oops, we can't seem to fetch current data!", error.data.message, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", error.data.message, "error", false)
            }
        }
    }


    createProduct = async ({ history }) => {
        this.getLoading = true;
        const { data, error } = await CreateProduct({
            name: this.editedData ? this.editedData.name : "",
            stock: this.editedData ? this.editedData.stock : "",
            description: this.editedData ? this.editedData.description : "",
            imageUrl: this.editedData ? this.editedData.imageUrl : "",
            price: this.editedData ? this.editedData.price : "",
        });
        this.getLoading = false;
        if (data) {
            this.swalModal("Success!", "Product created!", "success", false)
            history.push("/products")
        }
        else {
            if (error) {
                const message = error && error.data ? error.data.message : ""
                this.swalModal("Sorry!", message, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", "", "error", false)
            }
        }
    }

    updateProduct = async ({ productId, history }) => {
        this.getLoading = true;
        const { data, error } = await UpdateProduct({
            productId,
            name: this.editedData ? this.editedData.name : "",
            stock: this.editedData ? this.editedData.stock : "",
            description: this.editedData ? this.editedData.description : "",
            imageUrl: this.editedData ? this.editedData.imageUrl : "",
            price: this.editedData ? this.editedData.price : "",
            categoryId: this.editedData ? Number(this.editedData.categoryId) : "",
        })
        this.getLoading = false;
        if (data) {
            this.swalModal("Success!", "Product updated!", "success", false)
            history.push("/products")
        }
        else {
            if (error) {
                const message = error && error.data ? error.data.message : ""
                this.swalModal("Sorry!", message, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", "", "error", false)
            }
        }
    }
}

decorate(ProductStore, {
    getLoading: observable,
    editedData: observable,
    productArray: observable,

    getProducts: action,
    getProductById: action,
    createProduct: action,
    updateProduct: action,
    swalModal: action,
    setEditedData: action,
    clearData: action
});

export default new ProductStore();