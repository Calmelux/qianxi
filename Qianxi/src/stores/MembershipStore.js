import { observable, action, decorate, computed, toJS } from "mobx";
import { GetUsers, GetUserById, CreateUser, UpdateUser } from "../services/MembershipService"
import swal from "sweetalert2";

const initialUserData = {
    id: "",
    name: "",
    password: "",
    phoneNumber: "",
    postalCode: "",
    isActive: false,
    facebookToken: "",
    facebookId: "",
    facebookName: "",
    email: "",
    country: "",
    address: ""
}

class MembershipStore {
    getLoading = false;
    editedData = initialUserData;
    userArray = [];

    swalModal = (title, text, icon, showCancelButton) => {
        swal.fire({
            title: title,
            icon: icon,
            text: text,
            showCancelButton: showCancelButton
        })
    }

    clearData = () => {
        this.editedData = initialUserData;
    }

    setEditedData = updatedData => {
        this.editedData = {
            ...this.editedData,
            ...updatedData
        }
    }

    getUsers = async () => {
        this.getLoading = true;
        const { data, error } = await GetUsers({});
        this.getLoading = false;
        if (data) {
            this.userArray = data.docs.map((items) => {
                const id = items.id || "-";
                const name = items.name || "-";
                const phoneNumber = items.phoneNumber || "-";
                const address = items.address || "-";
                const email = items.email || "-";
                return { id: id, name: name, phoneNumber: phoneNumber, address: address, email: email }
            });
        } else {
            if (error) {
                return this.swalModal("Oops, we can't seem to fetch the data!", error, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", error, "error", false)
            }
        }
    }

    getUserById = async ({ userId }) => {
        this.getLoading = true;
        const { data, error } = await GetUserById({ userId });
        this.getLoading = false;
        if (data) {
            this.editedData.id = data.id || "-";
            this.editedData.name = data.name || "-"
            this.editedData.address = data.address || "-"
            this.editedData.email = data.email || "-"
            this.editedData.facebookId = data.facebookId || "-"
            this.editedData.facebookName = data.facebookName || "-"
            this.editedData.facebookToken = data.facebookToken || "-"
            this.editedData.password = data.password || "-"
            this.editedData.phoneNumber = data.phoneNumber || "-"
            this.editedData.postalCode = data.postalCode || "-"
        }
        else {
            if (error) {
                return this.swalModal("Oops, we can't seem to fetch current user's data!", error.data.message, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", error.data.message, "error", false)
            }
        }
    }


    createUser = async ({ history }) => {
        this.getLoading = true;
        const { data, error } = await CreateUser({
            name: this.editedData ? this.editedData.name : "",
            email: this.editedData ? this.editedData.email : "",
            password: this.editedData ? this.editedData.password : "",
            phoneNumber: this.editedData ? this.editedData.phoneNumber : "",
            address: this.editedData ? this.editedData.address : "",
            facebookId: this.editedData ? this.editedData.facebookId : "",
            facebookName: this.editedData ? this.editedData.facebookName : "",
            facebookToken: this.editedData ? this.editedData.facebookToken : "",
        });
        this.getLoading = false;
        if (data) {
            this.swalModal("Success!", "User created!", "success", false)
            history.push("/member")
        }
        else {
            if (error) {
                const message = error && error.data ? error.data.message : ""
                this.swalModal("Sorry!", message, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", "", "error", false)
            }
        }
    }

    updateUser = async ({ userId, history }) => {
        this.getLoading = true;
        const { data, error } = await UpdateUser({
            userId,
            name: this.editedData ? this.editedData.name : "",
            email: this.editedData ? this.editedData.email : "",
            password: this.editedData ? this.editedData.password : "",
            isActive: this.editedData ? this.editedData.isActive : true,
            creditAmount: this.editedData ? this.editedData.creditAmount : 1
        })
        this.getLoading = false;
        if (data) {
            this.swalModal("Success!", "User updated!", "success", false)
            history.push("/member")
        }
        else {
            if (error) {
                const message = error && error.data ? error.data.message : ""
                this.swalModal("Sorry!", message, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", "", "error", false)
            }
        }
    }
}


decorate(MembershipStore, {
    getLoading: observable,
    editedData: observable,
    userArray: observable,

    getUsers: action,
    getUserById: action,
    createUser: action,
    updateUser: action,
    swalModal: action,
    setEditedData: action,
    clearData: action
});

export default new MembershipStore();