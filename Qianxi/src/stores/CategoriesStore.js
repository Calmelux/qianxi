import { observable, action, decorate, computed, toJS } from "mobx";
import { GetCategories, GetCategoryById, CreateCategory, UpdateCategory } from "../services/CategoriesService"
import swal from "sweetalert2";

const initialCategoriesData = {
    id: "",
    name: "",
}

class CategoriesStore {
    getLoading = false;
    editedData = initialCategoriesData;
    categoryArray = [];

    swalModal = (title, text, icon, showCancelButton) => {
        swal.fire({
            title: title,
            icon: icon,
            text: text,
            showCancelButton: showCancelButton
        })
    }

    clearData = () => {
        this.editedData = initialCategoriesData;
    }

    setEditedData = updatedData => {
        this.editedData = {
            ...this.editedData,
            ...updatedData
        }
    }

    getCategories = async () => {
        this.getLoading = true;
        const { data, error } = await GetCategories({});
        this.getLoading = false;
        if (data) {
            this.categoryArray = data.docs.map((items) => {
                const id = items.id || "-";
                const name = items.name || "-";

                return { id: id, name: name }
            });
        } else {
            if (error) {
                return this.swalModal("Oops, we can't seem to fetch the data!", error, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", error, "error", false)
            }
        }
    }

    getCategoryById = async ({ categoryId }) => {
        this.getLoading = true;
        const { data, error } = await GetCategoryById({ categoryId });
        this.getLoading = false;
        if (data) {
            console.log("Data---", toJS(data));
            this.editedData.id = data.id;
            this.editedData.name = data.name;
        }
        else {
            if (error) {
                return this.swalModal("Oops, we can't seem to fetch current data!", error.data.message, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", error.data.message, "error", false)
            }
        }
    }


    createCategory = async ({ history }) => {
        this.getLoading = true;
        const { data, error } = await CreateCategory({
            name: this.editedData ? this.editedData.name : "",
        });
        this.getLoading = false;
        if (data) {
            this.swalModal("Success!", "Category created!", "success", false)
            history.push("/categories")
        }
        else {
            if (error) {
                const message = error && error.data ? error.data.message : ""
                this.swalModal("Sorry!", message, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", "", "error", false)
            }
        }
    }

    updateCategory = async ({ categoryId, history }) => {
        this.getLoading = true;
        const { data, error } = await UpdateCategory({
            categoryId,
            name: this.editedData ? this.editedData.name : "",
        })
        this.getLoading = false;
        if (data) {
            this.swalModal("Success!", "Category updated!", "success", false)
            history.push("/categories")
        }
        else {
            if (error) {
                const message = error && error.data ? error.data.message : ""
                this.swalModal("Sorry!", message, "error", false)
            }
            else {
                return this.swalModal("There's An Error Occured!", "", "error", false)
            }
        }
    }
}


decorate(CategoriesStore, {
    getLoading: observable,
    editedData: observable,
    categoryArray: observable,

    getCategories: action,
    getCategoryById: action,
    createCategory: action,
    updateCategory: action,
    swalModal: action,
    setEditedData: action,
    clearData: action
});

export default new CategoriesStore();