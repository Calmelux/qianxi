import { Login } from "../services/AuthService"
import { observable, action, decorate, computed, toJS } from "mobx";
import swal from "sweetalert2";
import { setCookie, deleteAllCookies } from "../helper/cookie";


const initialCredential = {
    email: "",
    password: ""
}

class AuthStore {

    credentials = initialCredential;

    setCredentials = (updatedData) => {
        this.credentials = {
            ...this.credentials,
            ...updatedData
        }
    }

    swalModal = (title, text, icon, showCancelButton) => {
        swal.fire({
            title: title,
            icon: icon,
            text: text,
            showCancelButton: showCancelButton
        })
    }

    login = async (history) => {
        const email = this.credentials.email;
        const password = this.credentials.password;
        let { data, error } = await Login({ email, password })

        if (data) {
            const token = data && data.token;
            setCookie('token', token);
            history.push("/member");
        }
        else {
            if (error) {
                const message = error.data ? error.data.message : ""
                this.swalModal("Sorry!", message, "error", false)
            }
            else {
                this.swalModal("Oops!", "There's Something Wrong!", "error", false)
            }
        }
    }

    logout = async (history) => {
        history.push("/login")
        await deleteAllCookies()
    }
}

decorate(AuthStore, {
    credentials: observable,
    token: observable,

    login: action,
    logout: action,
    setCredentials: action,
    swalModal: action
});
export default new AuthStore();