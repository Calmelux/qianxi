import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from 'prop-types';

class DefaultHeader extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };
    logout = async () => {
        const { history } = this.props;
        const { AuthStore } = this.props
        const { logout } = AuthStore;
        const { cookies } = this.props;

        await logout(history);
        await cookies.remove('token');
    }

    render() {
        return (
            <div className="header">
                <Row>
                    <Col sm="12" md="12" lg="12" style={{ height: "65px", display: "flex", justifyContent: "flex-end", alignItems: "center" }}>
                        <div style={{ cursor: "pointer", display: "flex", justifyContent: "center", alignItems: "center" }} onClick={() => this.logout()}>
                            <span style={{ margin: "20px", border: "1px solid black", borderRadius: "5px", padding: "3px" }}><span style={{ fontFamily: "Quicksand", fontSize: "17px", paddingRight: "5px", fontWeight: "bold" }}>Log Out</span><i className="fa fa-sign-out" ></i></span>
                        </div>
                    </Col>
                </Row>
            </div >
        )
    }
}
export default withCookies(withRouter(inject('AuthStore')(observer(DefaultHeader))));