import React, { Component } from "react";
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import routes from "../../routes"
import DefaultHeader from './DefaultHeader'
import { Container } from "@material-ui/core";
import { getCookie } from "../../helper/cookie";
import SideNav, { NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import { withCookies, Cookies } from 'react-cookie';

class DefaultLayout extends Component {

    render() {
        const token = getCookie('token')
        const { history } = this.props;
        if (!token) {
            return <Redirect to="/login" />
        }
        else {
            return (
                <div style={{ backgroundColor: "#E9E8E8", height: "100vh", width: "100vw" }}>
                    <div >
                        <DefaultHeader />
                    </div>
                    <div style={{ backgroundColor: "grey", zIndex: "99" }}>
                        <SideNav onSelect={(selected) => {
                            history.push("/" + selected);
                        }}>
                            <SideNav.Toggle />
                            <SideNav.Nav defaultSelected="member">
                                <NavItem eventKey="member">
                                    <NavIcon>
                                        <i className="fa fa-users" style={{ fontSize: '1.75em' }} />
                                    </NavIcon>
                                    <NavText>
                                        Member
                                    </NavText>
                                </NavItem>
                                <NavItem eventKey="categories">
                                    <NavIcon>
                                        <i className="fa fa-file" style={{ fontSize: '1.75em' }} />
                                    </NavIcon>
                                    <NavText>
                                        Category
                                    </NavText>
                                </NavItem>
                                <NavItem eventKey="products">
                                    <NavIcon>
                                        <i className="fa fa-archive" style={{ fontSize: '1.75em' }} />
                                    </NavIcon>
                                    <NavText>
                                        Products
                                    </NavText>
                                </NavItem>
                                <NavItem eventKey="orders">
                                    <NavIcon>
                                        <i className="fa fa-book" style={{ fontSize: '1.75em' }} />
                                    </NavIcon>
                                    <NavText>
                                        Order
                                    </NavText>
                                </NavItem>
                            </SideNav.Nav>
                        </SideNav>
                    </div>
                    <div style={{ marginLeft: "4rem" }}>
                        <Container maxWidth="xl">
                            <main className="main">
                                <Switch>
                                    {routes.map((route, idx) => {
                                        return route.component ? (
                                            <Route
                                                key={idx}
                                                path={route.path}
                                                exact={route.exact}
                                                name={route.name}
                                                render={props => <route.component {...props} />}
                                            />
                                        ) : null;
                                    })}
                                </Switch>
                            </main>
                        </Container>
                    </div>
                </div >
            );
        }
    }
}
export default withCookies(withRouter(DefaultLayout));