import React from "react";
import Loadable from "react-loadable";

import DefaultLayout from "./containers/DefaultLayout";
import LoadingCircular from "./components/LoadingCircular";

const Loading = ({ error, timedOut, pastDelay }) => {
    if (error || timedOut) {
        // To be caught by global ErrorBoundary
        throw new Error("Failed to load main routes chunk");
    } else if (pastDelay) {
        return <LoadingCircular />;
    } else {
        return null;
    }
};
//Login
const Login = Loadable({
    loader: () => import("./views/Login/Login"),
    loading: Loading
});

//Home
const Landing = Loadable({
    loader: () => import("./views/Landing/Landing"),
    loading: Loading
});

//Membership - Users
const Membership = Loadable({
    loader: () => import("./views/Membership/Membership"),
    loading: Loading
});

const MemberDetail = Loadable({
    loader: () => import("./views/Membership/MemberDetail"),
    loading: Loading
});

const CreateMember = Loadable({
    loader: () => import("./views/Membership/CreateMember"),
    loading: Loading
});

//Category

const Categories = Loadable({
    loader: () => import("./views/Categories/Categories"),
    loading: Loading
});

const CategoryDetail = Loadable({
    loader: () => import("./views/Categories/CategoryDetail"),
    loading: Loading
});

const CreateCategory = Loadable({
    loader: () => import("./views/Categories/CreateCategory"),
    loading: Loading
});

//Product
const Product = Loadable({
    loader: () => import("./views/Product/Product"),
    loading: Loading
});

const ProductDetail = Loadable({
    loader: () => import("./views/Product/ProductDetail"),
    loading: Loading
});

const CreateProduct = Loadable({
    loader: () => import("./views/Product/CreateProduct"),
    loading: Loading
});

//Order
const Order = Loadable({
    loader: () => import("./views/Order/Order"),
    loading: Loading
});

const OrderDetail = Loadable({
    loader: () => import("./views/Order/OrderDetail"),
    loading: Loading
});

const CreateOrder = Loadable({
    loader: () => import("./views/Order/CreateOrder"),
    loading: Loading
});

const routes = [
    {
        path: "/", exact: true, name: "Home", component: DefaultLayout
    },
    {
        path: "/login", exact: true, name: "Login", component: Login
    },
    {
        path: "/landing", exact: true, name: "Landing", component: Landing
    },
    //MEMBER
    {
        path: "/member", exact: true, name: "Member", component: Membership
    },
    {
        path: "/member/:userId", name: "Detail", component: MemberDetail
    },
    {
        path: "/create/user", name: "Create", component: CreateMember
    },
    //CATEGORY
    {
        path: "/categories", exact: true, name: "Category", component: Categories
    },
    {
        path: "/categories/:categoryId", name: "Detail", component: CategoryDetail
    },
    {
        path: "/create/category", name: "Create", component: CreateCategory
    },
    //PRODUCT
    {
        path: "/products", exact: true, name: "Product", component: Product,
    },
    {
        path: "/products/:productId", name: "Detail", component: ProductDetail,
    },
    {
        path: "/create/product", name: "Create", component: CreateProduct,
    },
    //ORDER
    {
        path: "/orders", exact: true, name: "Order", component: Order
    },
    {
        path: "/orders/:orderId", exact: true, name: "Detail", component: OrderDetail
    },
    {
        path: "/create/order", exact: true, name: "Create", component: CreateOrder
    }
]

export default routes;